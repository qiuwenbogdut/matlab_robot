function [x3,y3,z3,x4,y4,z4,danger_status,move_status,distance_3,distance_4]=DHfk3Dof_Lnya(th1,th2,th3,fcla)
%返回的结果是当前状态是否会与障碍物发生碰撞
%返回的变量表示的含义：
%safe_status：逻辑变量，机械臂是否与障碍物发生碰撞
%move_status：逻辑变量,机械臂是否已经到达了目的地
%distance：浮点型变量，表示机械臂末端与目标定之间的距离、


global Link

Build_3DOFRobot_Lnya;
radius    = 5;
len       = 10;
joint_col = 0;


%plot3(0,0,0,'ro'); 

 Link(2).th=th1*pi/180;
 Link(3).th=th2*pi/180;
 Link(4).th=th3*pi/180;
 

for i=1:4
Matrix_DH_Ln(i);
end

for i=2:4
      Link(i).A=Link(i-1).A*Link(i).A;
      Link(i).p= Link(i).A(:,4);
   
%画
%       Link(i).n= Link(i).A(:,1);
%       Link(i).o= Link(i).A(:,2);
%       Link(i).a= Link(i).A(:,3);
%       Link(i).R=[Link(i).n(1:3),Link(i).o(1:3),Link(i).a(1:3)];
      
% 画机械臂
%       Connect3D(Link(i-1).p,Link(i).p,'r',2); hold on;
%       DrawCylinder(Link(i-1).p, Link(i-1).R * Link(i).az, radius,len, joint_col); hold on;
end

axis3_point=Link(4).p(1:3,:);%第三个轴的末端位置
x4=axis3_point(1);
y4=axis3_point(2);
z4=axis3_point(3);

axis3_point=Link(3).p(1:3,:);%第三个轴的起始端位置
x3=axis3_point(1);
y3=axis3_point(2);
z3=axis3_point(3);


% 画坐标系
% grid on;
% axis([-200,200,-200,200,-200,200]);
% xlabel('x');
% ylabel('y');
% zlabel('z');


danger_status=drowshit(40,30,30,20) | drowshit(70,-20,60,20); %画障碍物,并判断有没有发生碰撞
[move_status,distance_3,distance_4]=drowgoal()  ;                       %画画起点和目标点，并判断是否已经到了目标点,末端点与目标点之间的距离

%画
% drawnow;
if(fcla)
    cla;% cla 清屏
end


end

function danger_status=drowshit(x,y,z,r)
 % 该函数用于画障碍物,并进行碰撞检测，画一个检测一次，并输出结果
 %画
% [a,b,c]=sphere(40);
% X=a*r+x;
% Y=b*r+y;
% Z=c*r+z;
% mesh(X,Y,Z);hold on;


danger_status=check_safe(x,y,z,r);

end 

function [move_status,distance_3,distance_4]=drowgoal()
global Link

r=10;
%画
% %起点坐标   118.3013  -68.3013  13.3975
% [a,b,c]=sphere(40);
% 
% X=a*r+118.3013;
% Y=b*r+ (-68.3013);
% Z=c*r+13.3975;
% mesh(X,Y,Z);hold on;
% 
%终点坐标   50,5,30,
% X=a*r+50;
% Y=b*r+ 5;
% Z=c*r+30;
% mesh(X,Y,Z);hold on;

goal_point=[50;5;30];
distance_4=norm(Link(4).p(1:3,:)-goal_point);
distance_3=norm(Link(3).p(1:3,:)-goal_point);
if  distance_4<=r
    move_status=true;
else
    move_status=false;
end

end
