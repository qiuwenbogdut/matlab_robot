%逆运动学应用的例子，空间中画一个圆
clc;
close all;
clear;

ToDeg = 180/pi;
ToRad = pi/180;
L1=50;
L2=100;
L3=100;


view(200,20);

px=-100; py=50; pz=100;

[th1,th2,th3]=IK_3DOF_Rob_Lnya(L1,L2,L3,px,py,pz);
th1=th1*ToDeg;
th2=th2*ToDeg;

th3=th3*ToDeg;

DHfk3Dof_Lnya(th1,th2,th3,0); 
plot3(px,py,pz,'r*');

%pause;

%%
k=1;

for t = 0 : 2*ToRad  :360*ToRad
    
   cx=100;
   cy=100;
   cz=70;
    r=40;
    n=[1,1,0.5,];
    

   u=[n(2),  -n(1),  0 ] ;
   v=[ n(1)*n(3),     n(2)*n(3),    (-n(1)^2-n(2)^2)   ];
  x(k)=cx + r * ( u(1) * cos(t) + v(1) * sin(t) )  ;
  y(k)=cy + r * ( u(2) * cos(t) + v(2) * sin(t) )  ;
  z(k)=cz + r * ( u(3) * cos(t) + v(3) * sin(t) )   ;
  plot3(x,y,z,'RX');hold on;  

  
  
  
  [th1,th2,th3]=IK_3DOF_Rob_Lnya(L1,L2,L3,x(k),y(k),z(k));
  
  k=k+1;
  
th1=th1*ToDeg;
th2=th2*ToDeg;
th3=th3*ToDeg;
 
 DHfk3Dof_Lnya(th1,th2,th3,1);
 

end

hold on;


for i= 0 : 0.02 : 1
    
    x(k)=-80*i+140;
    y(k)=75*i+60;
    z(k)=10*i+70;
    
    plot3(x,y,z,'kX'); 
    [th1,th2,th3]=IK_3DOF_Rob_Lnya(L1,L2,L3,x(k),y(k),z(k));
    th1=th1*ToDeg;
th2=th2*ToDeg;
th3=th3*ToDeg;
    k=k+1;
     
   if i ==1
     DHfk3Dof_Lnya(th1,th2,th3,0);
   else
     DHfk3Dof_Lnya(th1,th2,th3,1);
   end
    
end



