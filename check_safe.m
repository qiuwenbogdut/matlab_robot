function danger_status=check_safe(x0,y0,z0,r)
%输入的是球形障碍物的圆心
global Link

danger_status=0;



for i=2:4 %对每一条杆进行判断，看有没有发生碰撞,首先判断障碍物与杆的距离，然后判断杆两端与障碍物的距离。

    Q1=[x0;y0;z0];
    position_start=Link(i-1).p(1:3,:); %获取杆的第一个坐标
    position_end=Link(i).p(1:3,:);     %获取杆的最后一个坐标

    if (x0-position_start(1))*(x0-position_end(1))<0 &&   (y0-position_start(2))*(y0-position_end(2))<0 &&   (z0-position_start(3))*(z0-position_end(3))<0
        %内
        r3=norm(cross( position_end-position_start,Q1-position_start))/norm( position_end-position_start); 
        if r3>r
            danger_status=0;
        else
            danger_status=1;
        end
        
        
    else
        %外
        r4=norm(position_start-Q1);
        r5=norm(position_end-Q1);
        
        if r4>r && r5>r
            danger_status=0;
        else
            danger_status=1;
        end
        
    end
    
    
    if danger_status==1
        break
    end
   
end


end